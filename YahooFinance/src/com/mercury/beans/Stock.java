package com.mercury.beans;

public class Stock {
	private String sid;
	private double price;
	private double change;
	private double prev_close;
	private double open;
	private double volume;
	private double day_low;
	private double day_high;
	private String name;
	
	public Stock() {}
	public Stock(String sid) {
		this.sid = sid;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSid() {
		return sid;
	}
	public void setSid(String sid) {
		this.sid = sid;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	public double getChange() {
		return change;
	}
	public void setChange(double change) {
		this.change = change;
	}
	public double getPrev_close() {
		return prev_close;
	}
	public void setPrev_close(double prev_close) {
		this.prev_close = prev_close;
	}
	public double getOpen() {
		return open;
	}
	public void setOpen(double open) {
		this.open = open;
	}
	public double getVolume() {
		return volume;
	}
	public void setVolume(double volume) {
		this.volume = volume;
	}
	public double getDay_low() {
		return day_low;
	}
	public void setDay_low(double day_low) {
		this.day_low = day_low;
	}
	public double getDay_high() {
		return day_high;
	}
	public void setDay_high(double day_high) {
		this.day_high = day_high;
	}

	
}
