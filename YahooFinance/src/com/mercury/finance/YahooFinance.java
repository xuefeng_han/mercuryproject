package com.mercury.finance;

import java.io.*;
import java.net.*;
import java.util.List;

import com.mercury.beans.Stock;

public class YahooFinance {
	private static void getPrice(Stock stock) {
		String yahoo_quote = "http://finance.yahoo.com/d/quotes.csv?s=" + stock.getSid() + "&f=sc1l1povghn&e=.c";
		double price = 0;
		double change = 0;
		double prev_close=0;
		double open=0;
		double volume=0;
		double day_low=0;
		double day_high=0;
		String name="";
		try {
			URL url = new URL(yahoo_quote);
			URLConnection urlconn = url.openConnection();
			BufferedReader in = new BufferedReader(new InputStreamReader(urlconn.getInputStream()));
			String content = in.readLine();
			content = content.replace((char)34, (char)32);
			String[] tokens = content.split(",");
			change = Double.parseDouble(tokens[1].trim());
			price = Double.parseDouble(tokens[2].trim());
			prev_close= Double.parseDouble(tokens[3].trim());
			open= Double.parseDouble(tokens[4].trim());
			volume = Double.parseDouble(tokens[5].trim());
			day_low= Double.parseDouble(tokens[6].trim());
			day_high = Double.parseDouble(tokens[7].trim());
			name=tokens[8].trim();
			for(int i=9;i<=tokens.length-1;i++){
				name = name+"," + tokens[i];
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		stock.setPrice(price);
		stock.setChange(change);
		stock.setPrev_close(prev_close);
		stock.setOpen(open);
		stock.setVolume(volume);
		stock.setDay_low(day_low);
		stock.setDay_high(day_high);
		stock.setName(name);
	}
	public static void marketData(List<Stock> stocks) {
		for (Stock stock:stocks) {
			getPrice(stock);
		}
	}
}
